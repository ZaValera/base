import * as React from 'react';

import {SomeComponent} from 'front/components/someComponent/SomeComponent';


export default {
    title: 'SomeComponent',
    component: SomeComponent,
};

export const Primary = () => <SomeComponent text={'Primary'}/>;
export const Second = () => <SomeComponent text={'Second'}/>;