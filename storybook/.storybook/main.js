const webpackConfig = require('../../web/src/webpack.front.config');
const path = require('path');

module.exports = {
    stories: ['../stories/**/*.[tj]sx'],
    // stories: ['../stories/**/*.@(js|jsx|ts|tsx)'],
    addons: ['@storybook/addon-essentials'],
    // framework: '@storybook/react',
    core: {
        builder: 'webpack5',
    },
    features: {
        storyStoreV7: true,
    },
    webpackFinal: (config) => {
        const mainConfig = webpackConfig({
            dev: false,
            dirname: path.resolve(__dirname, '../../web/src'),
            storybook: true,
        });

        return {
            ...config,
            module: {
                ...config.module,
                rules: [
                    config.module.rules[0],
                    ...mainConfig.module.rules.slice(1),
                ],
            },
            resolve: {
                ...config.resolve,
                extensions: [
                    ...config.resolve.extensions,
                    ...mainConfig.resolve.extensions,
                ],
                modules: [
                    ...config.resolve.modules,
                    ...mainConfig.resolve.modules,
                ],
                alias: {
                    ...config.resolve.alias,
                    ...mainConfig.resolve.alias,
                },
            },
            plugins: [
                ...config.plugins,
                ...mainConfig.plugins,
            ],
        };
    },
};