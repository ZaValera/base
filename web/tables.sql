CREATE TABLE IF NOT EXISTS item (
    id INTEGER NOT NULL PRIMARY KEY,
    date INTEGER NOT NULL default 0,
    price REAL NOT NULL default 0,
    value INTEGER NOT NULL default 0
);

CREATE TABLE IF NOT EXISTS description (
    id INTEGER NOT NULL PRIMARY KEY,
    description TEXT NOT NULL
);
