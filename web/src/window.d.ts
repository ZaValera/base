import moment from 'moment';
import {configureStore} from '@reduxjs/toolkit';


declare global {
    interface Window {
        moment: typeof moment;
        store: ReturnType<typeof configureStore>;
    }
}
