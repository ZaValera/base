const path = require('path');
const nodeExternals = require('webpack-node-externals');
const {getSettings, getParams, getPlugins} = require('./webpack.utils');


const TYPE = 'back';

module.exports = env => {
    const {
        dirname,
    } = getParams(env);

    return {
        ...getSettings(env, TYPE),
        plugins: getPlugins(env, TYPE),
        entry: './src/back/index.ts',
        output: {
            filename: 'index.js',
            path: path.resolve(dirname, '../build/back'),
            publicPath: '/build/',
            globalObject: 'this',
        },
        node: {
            __dirname: false,
        },
        externals: [nodeExternals()],
    };
};
