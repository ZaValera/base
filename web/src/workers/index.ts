import workerpool from 'workerpool';

async function doSomething(options: any) {
    return 'result';
}

workerpool.worker({
    doSomething,
});
