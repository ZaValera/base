import 'front/styles/index.css';
import React from 'react';
import {Provider} from 'react-redux'
import AdapterDateFns from '@mui/lab/AdapterMoment';
import {LocalizationProvider} from '@mui/x-date-pickers/LocalizationProvider';
import {QueryClient, QueryClientProvider} from 'react-query';
import {Layout} from 'front/layout/Layout';
import {store} from './store';


const queryClient = new QueryClient({
    defaultOptions: {
        queries: {
            refetchOnWindowFocus: false,
            staleTime: Infinity,
            retry: false,
        },
    },
});

export function App() {
    return (
        <React.StrictMode>
            <Provider store={store}>
                <QueryClientProvider client={queryClient}>
                    <LocalizationProvider dateAdapter={AdapterDateFns}>
                        <Layout/>
                    </LocalizationProvider>
                </QueryClientProvider>
            </Provider>
        </React.StrictMode>
    );
}