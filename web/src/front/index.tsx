import React from 'react';
import {hydrateRoot, createRoot} from 'react-dom/client';
import {BrowserRouter} from 'react-router-dom';
import {App} from './app';
import {store} from './store';
import {createEmotionCache, theme} from 'shared/mui';
import CssBaseline from '@mui/material/CssBaseline';
import {ThemeProvider} from '@mui/material/styles';
import {CacheProvider} from '@emotion/react';
import moment from 'moment';


const cache = createEmotionCache();
window.store = store;
window.moment = moment;
moment.locale('ru');

const rootEl = document.getElementById('root');

const Root = () => (
    <BrowserRouter>
        <CacheProvider value={cache}>
            <ThemeProvider theme={theme}>
                <CssBaseline />
                <App />
            </ThemeProvider>
        </CacheProvider>
    </BrowserRouter>
);

if (process.env.NODE_ENV === 'development') {
    createRoot(rootEl).render(<Root/>);
} else {
    hydrateRoot(rootEl, <Root/>);
}
