import React from 'react';
import {Router} from 'front/pages/Router';
import {GlobalContext, useGlobalSlice} from 'front/globalSlice';
import {Head} from './head/Head';
import styles from './layout.scss';


export function LayoutComponent() {
    const slice = useGlobalSlice('global');

    return (
        <GlobalContext.Provider value={slice}>
            <div className={styles.main}>
                <Head/>
                <div className={styles.body}>
                    <Router/>
                </div>
            </div>
        </GlobalContext.Provider>
    );
}

export const Layout = LayoutComponent;
