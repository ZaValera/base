import React, {useRef, useEffect} from 'react';
import {Route, Link, Routes, useLocation} from 'react-router-dom';
import Toolbar from '@mui/material/Toolbar';
import Menu from '@mui/material/Menu';
import AppBar from '@mui/material/AppBar';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';
import MenuItem from '@mui/material/MenuItem';
import MenuIcon from '@mui/icons-material/Menu';
import {GlobalContext} from 'front/globalSlice';
import {useSlice} from 'front/sliceHook';
import styles from './head.scss';


export function Head() {
    const {state, actions} = useSlice(GlobalContext);
    const {showMenu} = state;
    const {setShowMenu} = actions;
    const location = useLocation();

    const handleClick = () => {
        setShowMenu(true);
    };

    const handleClose = () => {
        setShowMenu(false);
    };

    const menuButtonRef = useRef(null);

    useEffect(() => {
        setShowMenu(false);
    }, [location, setShowMenu]);

    return (
        <AppBar position='relative' style={{zIndex: 1}}>
            <Toolbar>
                <IconButton edge='start' color='inherit' aria-label='menu' onClick={handleClick} ref={menuButtonRef}>
                    <MenuIcon/>
                </IconButton>
                {menuButtonRef.current && <Menu
                    anchorEl={menuButtonRef.current}
                    keepMounted
                    open={showMenu}
                    onClose={handleClose}
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'center',
                    }}
                    transformOrigin={{
                        vertical: 'top',
                        horizontal: 'center',
                    }}
                >
                    <MenuItem>
                        <Link className={styles.link} to='/'>Main</Link>
                    </MenuItem>
                    <MenuItem>
                        <Link className={styles.link} to='/sub'>Sub</Link>
                    </MenuItem>
                </Menu>}
                <Typography variant='h6' className={styles.title}>
                    <Routes>
                        <Route path='/' element={<div>Main</div>}/>
                        <Route path='/sub' element={<div>Sub</div>}/>
                    </Routes>
                </Typography>
            </Toolbar>
        </AppBar>
    );
}
