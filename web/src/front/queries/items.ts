import {useQuery} from 'react-query'
import axios from 'axios';
import {IItem} from 'shared/consts';


export const useItems = (params?: {from: number; to: number}) => {
    const {from, to} = params || {};

    return useQuery<IItem[]>(['useCandles', from, to], async () => {
        return await getItems(from, to);
    }, {
        enabled: Boolean(params),
    });
};

const getItems = async (from: number, to: number): Promise<IItem[]> => {
    const {data} = await axios({
        method: 'get',
        url: '/api/v1/items.json',
        params: {from, to},
    });

    return data;
};

const fetchItems = async (from: number, to: number): Promise<IItem[]> => {
    const {data} = await axios({
        method: 'post',
        url: '/api/v1/items.json',
        data: {from, to}
    });

    return data;
};

const deleteItems = async (from: number, to: number) => {
    await axios({
        method: 'delete',
        url: '/api/v1/items.json',
        data: {from, to}
    });
};
