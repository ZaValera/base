import {useQuery, useMutation, useQueryClient} from 'react-query'
import axios from 'axios';
import {IDescription} from 'shared/consts';

export const useDescription = (id: number) => {
    return useQuery<IDescription>(['getDescription'], async () => {
        return await getDescription(id);
    });
};

export const useDescriptions = () => {
    return useQuery<IDescription[]>(['getDescriptions'], async () => {
        return await getDescriptions();
    });
};

export const useAddDescription = () => {
    const queryClient = useQueryClient();
    const {data: descriptions} = useDescriptions();

    return useMutation<IDescription, unknown, IDescription>(async (params) => {
        return await addDescription(params);
    }, {
        onSuccess: (data) => {
            queryClient.setQueryData(['getDescriptions'], [...descriptions, data]);
        },
    });
}

export const useDeleteDescription = () => {
    const queryClient = useQueryClient();
    const {data: descriptions} = useDescriptions();

    return useMutation<void, unknown, {id: number}>(async (params) => {
        await deleteDescription(params.id);
    }, {
        onSuccess: (data, params) => {
            queryClient.setQueryData(['getDescriptions'], descriptions.filter(item => item.id !== params.id));
        },
    });
}

const getDescription = async (id: number): Promise<IDescription> => {
    const {data} = await axios({
        method: 'get',
        url: `/api/v1/descriptions/${id}.json`,
    });

    return data;
};

const getDescriptions = async (): Promise<IDescription[]> => {
    const {data} = await axios({
        method: 'get',
        url: '/api/v1/descriptions.json',
    });

    return data;
};

const addDescription = async (params: IDescription): Promise<IDescription> => {
    const {data} = await axios({
        method: 'post',
        url: '/api/v1/descriptions.json',
        data: params,
    });

    return data;
};

const deleteDescription = async (id: number) => {
    await axios({
        method: 'delete',
        url: `/api/v1/descriptions/${id}.json`,
    });
};
