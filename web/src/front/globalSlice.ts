import React from 'react';
import {PayloadAction} from '@reduxjs/toolkit';
import {createUseSliceHook} from 'front/sliceHook';


const initialState: ISliceState = {
    isLoading: true,
    showMenu: false,
};

const reducers = {
    setIsLoading(state: ISliceState, action: PayloadAction<boolean>) {
        state.isLoading = action.payload;
    },
    setShowMenu(state: ISliceState, action: PayloadAction<boolean>) {
        state.showMenu = action.payload;
    },
};

export const useGlobalSlice = createUseSliceHook({
    name: 'global',
    initialState,
    reducers,
});

export const GlobalContext = React.createContext<TSlice>(null);

interface ISliceState {
    isLoading: boolean;
    showMenu: boolean;
}

type TSlice = ReturnType<typeof useGlobalSlice>;
