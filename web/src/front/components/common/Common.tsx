import React from 'react';
import styles from './common.scss';
import {DATE_FORMAT} from 'shared/consts';
import moment from 'moment';

export function Common(props: IProps) {
    return (
        <div className={styles.common}>
            Common Component {props.text} {moment().format(DATE_FORMAT)}
        </div>
    );
}

interface IProps {
    text: string;
}
