import {
    configureStore,
    combineReducers,
    Reducer,
    ReducersMapObject,
} from '@reduxjs/toolkit';


const initialReducers: ReducersMapObject = {
    init: () => ({}), // заглушка, так как стора пустой быть не должна, ругаетяс ReduxDevtools
    // здесь перечисляются внешние редьюсеры
};

export const store = configureStore({
    reducer: combineReducers(initialReducers),
    /* middleware: (getDefaultMiddleware) => {
        return getDefaultMiddleware({
            serializableCheck: {
                ignoreActions: true,
            },
        });
    }, */
});

const reducers = {...initialReducers};

export const reducerManager = {
    add: (key: string, reducer: Reducer) => {
        if (!key || reducers[key]) {
            return;
        }

        reducers[key] = reducer;

        store.replaceReducer(combineReducers(reducers));
    },

    remove: (key: string) => {
        if (!key || !reducers[key]) {
            return;
        }

        delete reducers[key];
        store.replaceReducer(combineReducers(reducers));
    },
};

export type TRootState = ReturnType<typeof store.getState>
