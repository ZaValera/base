import React from 'react';
import {PayloadAction} from '@reduxjs/toolkit';
import {createUseSliceHook} from 'front/sliceHook';

const initialState: ISliceState = {
    count: 0,
};

const reducers = {
    increase(state: ISliceState) {
        state.count++;
    },
    decrease(state: ISliceState) {
        state.count--;
    },
    setCount(state: ISliceState, action: PayloadAction<ISliceState['count']>) {
        const newCount = action.payload;

        if (newCount !== null) {
            state.count = action.payload;
        }
    },
};

export const useMainSlice = createUseSliceHook({
    name: 'main',
    initialState,
    reducers,
});

export const MainContext = React.createContext<TSlice>(null);

export interface ISliceState {
    count: number;
}

type TSlice = ReturnType<typeof useMainSlice>;
