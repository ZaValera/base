import React, {useState} from 'react';
import styles from './mainPage.scss';
import moment from 'moment';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import TextField from '@mui/material/TextField';
import {useItems} from 'front/queries/items';
import {MainContext, useMainSlice} from './mainSlice';
import {Controls} from './controls/Controls';
import {useSlice} from 'front/sliceHook';


export function MainPage() {
    const slice = useMainSlice('main');
    const {state, actions} = useSlice(slice);
    const [newCount, setNewCount] = useState<number>(null);

    const items = useItems({
        from: moment().subtract(-3).startOf('day').unix(),
        to: moment().startOf('day').unix(),
    });

    return (
        <MainContext.Provider value={slice}>
            <Stack
                className={styles.mainPage}
                spacing={2}
            >
                <Controls/>
                <Stack spacing={2} direction='row'>
                    <TextField
                        className={styles.input}
                        label='Count'
                        size='small'
                        variant='outlined'
                        value={newCount ?? ''}
                        onChange={(e) => {
                            const value = e.target.value;

                            if (!value) {
                                setNewCount(null);

                                return;
                            }

                            const n = Number(value);

                            if (isNaN(n)) {
                                return;
                            }

                            setNewCount(n);
                        }}
                    />
                    <Button
                        variant='contained'
                        size='small'
                        color='success'
                        style={{
                            width: 100,
                        }}
                        onClick={() => {
                            actions.setCount(newCount);
                        }}
                    >
                        Set Count
                    </Button>
                </Stack>
                <div className={styles.bigText}>Count: {state.count}</div>
            </Stack>
        </MainContext.Provider>
    );
}
