import React from 'react';
import styles from './controls.scss';
import Button from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import {useSlice} from 'front/sliceHook';
import {MainContext} from 'front/pages/main/mainSlice';


export function Controls(props: IProps) {
    const {actions} = useSlice(MainContext);
    const {increase, decrease} = actions;

    return (
        <Stack
            className={styles.someComponent}
            spacing={2}
            direction={'row'}
        >
            <Button
                className={styles.button}
                variant='outlined'
                size='small'
                color='primary'
                onClick={() => {
                    decrease();
                }}
            >
                Decrease
            </Button>
            <Button
                className={styles.button}
                variant='contained'
                size='small'
                color='primary'
                onClick={() => {
                    increase();
                }}
            >
                Increase
            </Button>
        </Stack>
    );
}

interface IProps {

}
