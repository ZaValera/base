import React from 'react';
import styles from './subComponent.scss';

export function SubComponent(props: IProps) {
    return (
        <div className={styles.subComponent}>
            Sub Component
        </div>
    );
}

interface IProps {

}
