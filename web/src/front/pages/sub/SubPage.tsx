import React from 'react';
import styles from './subPage.scss';
import {useDescriptions} from 'front/queries/description';
import {SubContext, useSubSlice} from './subSlice';
import {SubComponent} from './subComponent/SubComponent';
import {Common} from 'front/components/common/Common';
import {useSlice} from 'front/sliceHook';


export function SubPage() {
    const slice = useSubSlice('sub');
    const {state, actions} = useSlice(slice);
    const descriptions = useDescriptions();

    return (
        <SubContext.Provider value={slice}>
            <div className={styles.subPage}>
                <SubComponent />
                <Common text='Sub level'/>
            </div>
        </SubContext.Provider>
    );
}
