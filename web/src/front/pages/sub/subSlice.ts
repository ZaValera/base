import React from 'react';
import {PayloadAction} from '@reduxjs/toolkit';
import {createUseSliceHook} from 'front/sliceHook';
import {DATE_FORMAT} from 'shared/consts';
import moment from 'moment';

const initialState: ISliceState = {
    date: moment().format(DATE_FORMAT),
};

const reducers = {
    setDate(state: ISliceState, action: PayloadAction<string>) {
        state.date = action.payload;
    },
};

export const useSubSlice = createUseSliceHook({
    name: 'sub',
    initialState,
    reducers,
});

export const SubContext = React.createContext<TSlice>(null);

export interface ISliceState {
    date: string;
}

type TSlice = ReturnType<typeof useSubSlice>;
