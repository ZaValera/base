import React, {lazy, Suspense} from 'react';
import {Route, Routes} from 'react-router-dom';
import CircularProgress from '@mui/material/CircularProgress';
import MainPage from 'front/pages/main';
import SubPage from 'front/pages/sub';


// const MainPage = lazy(() => import(
//     /* webpackChunkName: "pages/MainPage" */
//     'front/pages/main'
//     )
// );

// const SubPage = lazy(() => import(
//     /* webpackChunkName: "pages/SubPage" */
//     'front/pages/sub'
//     )
// );

export function Router() {
    return (
        // <Suspense fallback={<CircularProgress />}>
            <Routes>
                <Route path='/' element={<MainPage/>}/>
                <Route path='/sub' element={<SubPage/>}/>
            </Routes>
        // </Suspense>
    );
}