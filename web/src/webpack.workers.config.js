const path = require('path');
const {getSettings, getParams, getPlugins} = require('./webpack.utils');


const TYPE = 'workers';

module.exports = env => {
    const {dirname} = getParams(env);

    return {
        ...getSettings(env, TYPE),
        plugins: getPlugins(env, TYPE),
        entry: './src/workers/index.ts',
        output: {
            filename: 'index.js',
            path: path.resolve(dirname, '../build/workers'),
            publicPath: '/build/',
        },
    };
};