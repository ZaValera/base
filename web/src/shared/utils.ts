import moment from 'moment';

export function getHumanTime(time: number) {
    let distance: string;

    if (time < 1000) {
        distance = `${time}ms`;
    } else if (time < 60 * 1000) {
        distance = `${round(time / 1000, 2)}s`;
    } else if (time < 60 * 60 * 1000) {
        distance = `${round(time / (60 * 1000), 2)}min`;
    } else {
        distance = `${round(time / (60 * 60 * 1000), 2)}h`;
    }

    return distance;
}

export function getLogString(pref: string, str: string, timeStart?: number) {
    const res = [`${now()}:`, `[${pref}]`, str];

    if (timeStart) {
        const time = Date.now() - timeStart;

        res.push(`(${getHumanTime(time)})`);
    }

    return res.join(' ');
}

export function log(pref: string, str: string, timeStart?: number) {
    console.log(getLogString(pref, str, timeStart));
}

export function now() {
    return `${moment().format('DD.MM.YYYY HH:mm:ss')}`;
}

export function round(n: number, precision?: number) {
    const factor = Math.pow(10, precision || 0);

    return Math.round(n * factor) / factor;
}

export function sleep(time: number) {
    return new Promise((suceess) => {
        setTimeout(suceess, time);
    });
}

export function splitNumberRange(n: string|number, options?: {sep: string}) {
    const nStr = String(n);
    const [int, fract] = nStr.split(/[.,]/);
    let count = 0;
    let res = [];

    for (const l of int.split('').reverse()) {
        if (count === 3) {
            res.unshift(' ');
            count = 0;
        }

        res.unshift(l);
        count++;
    }

    if (fract) {
        res.push(options?.sep || '.', fract);
    }

    return res.join('');
}
