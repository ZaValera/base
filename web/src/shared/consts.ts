import moment from 'moment';

export const DATE_FORMAT = 'DD.MM.YYYY';

export const DATE_TIME_FORMAT = 'DD.MM.YYYY HH:mm';

export const UNIX_FORMAT = 'X';


export enum EDbTables {
    item = 'item',
    description = 'description',
}

export type TSQLiteSchema = {
    [key: string]: 'INTEGER'|'REAL'|'TEXT'|'BLOB';
};

export interface IItem {
    id?: number;
    date: number;
    price: number;
    value: number;
}

export interface IDescription {
    id?: number;
    description: string;
}
