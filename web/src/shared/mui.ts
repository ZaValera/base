import {createTheme} from '@mui/material/styles';
import createCache from '@emotion/cache';

export const theme = createTheme({
    typography: {
        fontFamily: 'Montserrat, sans-serif',
        fontWeightMedium: 500,
    },
    components: {
        /* MuiCssBaseline: {
            styleOverrides: {
                body: {
                    backgroundColor: '#fafafa',
                }
            }
        }, */
        MuiButton: {
            styleOverrides: {
                root: {
                    fontWeight: 500,
                },
            }
        },
    },
});

export function createEmotionCache() {
    return createCache({key: 'css', prepend: true});
}