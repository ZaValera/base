import sqlite from 'sqlite3';
import path from 'path';
import fs from 'fs';
import os from 'os';
import {log} from 'back/backUtils';


// const maxVariables = 32766;
const dbPath = path.join(os.tmpdir(), 'base.db');

export const errCb = (err: Error) => {
    if (err) {
        log('sqlite error', err.message);
    }
}

export const getDb = () => {
    return new sqlite.Database(dbPath, errCb);
};

export const dbInit = (cb: () => void) => {
    const db = getDb();

    const initSqls = fs.readFileSync(path.resolve(__dirname, '../../tables.sql'), 'utf8')
        .trim()
        .split(/;[\n\s]*/)
        .filter(Boolean);

    db.serialize(function () {
        db.run('BEGIN TRANSACTION', errCb);

        for (const sql of initSqls) {
            db.run(sql, errCb);
        }

        db.run('COMMIT', errCb);
        db.run('PRAGMA shrink_memory', errCb);
        log('sqlite init', 'SUCCESS');
    });

    db.close((err) => {
        if (err) {
            errCb(err);
        } else {
            cb();
        }
    });
};
