import {EDbTables, TSQLiteSchema, IDescription} from 'shared/consts';
import {remove, select, addRows} from 'back/SQLite';
import {log} from 'back/backUtils';


const schema: TSQLiteSchema = {
    description: 'TEXT',
};

export async function getDescriptions() {
    try {
        const queryString = `SELECT * FROM ${EDbTables.description};`;

        return await select<IDescription>(queryString);
    } catch (e) {
        throw log('sql', `ERROR: ${e}`);
    }
}

export async function deleteDescriptions() {
    try {
        const queryString = `DELETE FROM ${EDbTables.description};`;

        await remove(queryString);
    } catch (e) {
        throw log('sql', `ERROR: ${e}`);
    }
}

export async function addDescriptions(items: IDescription[]) {
    try {
        await addRows(EDbTables.description, schema, items);
    } catch (e) {
        throw log('sql', `ERROR: ${e}`);
    }
}
