import {EDbTables, IItem, TSQLiteSchema} from 'shared/consts';
import {remove, select, addRowsChunk} from 'back/SQLite';
import {log} from 'back/backUtils';


const schema: TSQLiteSchema = {
    date: 'INTEGER',
    price: 'REAL',
    value: 'INTEGER',
};

export async function getItems(from: number, to: number) {
    try {
        const queryString = `SELECT * FROM ${EDbTables.item} where date >= "${from}" AND date <= "${to}";`;

        return await select<IItem>(queryString);
    } catch (e) {
        throw log('sql', `ERROR: ${e}`);
    }
}

export async function* getItemsChunked(from: number, to: number) {
    try {
        const month = 2592000;
        const queryString = `SELECT * FROM ${EDbTables.item}`;

        for (let i = from; i <= to;) {
            const curFrom = i;
            let curTo = i + month - 1;

            if (curTo > to) {
                curTo = to;
            }

            i += month;

            yield await select<IItem>(`${queryString} AND date >= "${curFrom}" AND date <= "${curTo}";`);
        }
    } catch (e) {
        throw log('sql', `ERROR: ${e}`);
    }
}

export async function deleteItems(from: number, to: number) {
    try {
        const queryString = `DELETE FROM ${EDbTables.item} where date >= "${from}" AND date <= "${to}";`;

        await remove(queryString);
    } catch (e) {
        throw log('sql', `ERROR: ${e}`);
    }
}

export async function addItems(items: IItem[]) {
    try {
        await addRowsChunk(EDbTables.item, schema, items);
    } catch (e) {
        throw log('sql', `ERROR: ${e}`);
    }
}
