import {EDbTables, TSQLiteSchema} from 'shared/consts';
import {log} from 'back/backUtils';
import {getDb, errCb} from './core';


export function addRow(table: EDbTables, schema: TSQLiteSchema, item: {[key: string]: any}): Promise<number> {
    return new Promise(function (resolve, reject) {
        const db = getDb();
        let res: any;

        db.serialize(function () {
            db.run('BEGIN TRANSACTION', errCb);

            const keys = Object.keys(schema);
            const values = [];

            for (const key of keys) {
                values.push(item[key]);
            }

            const sql = `INSERT INTO ${table} (${keys.join(',')}) VALUES (${Array(keys.length).fill('?').join(',')})`;

            db.run(sql, values, function (err) {
                if (err) {
                    reject(err.message);
                } else {
                    res = this.lastID;
                }
            });

            db.run('COMMIT', errCb);
            db.run('PRAGMA shrink_memory', errCb);
        });

        db.close(function (err) {
            if (err) {
                reject(err.message);
            } else {
                resolve(res);
            }
        });
    });
}

export function addRows(table: EDbTables, schema: TSQLiteSchema, items: Array<{[key: string]: any}>): Promise<number> {
    return new Promise(function (resolve, reject) {
        const db = getDb();
        let res: any;

        db.serialize(function () {
            db.run('BEGIN TRANSACTION', errCb);

            const keys = Object.keys(schema);
            let rows = [];

            for (const item of items) {
                const values = [];

                for (const key of keys) {
                    values.push(cover(item[key], schema[key]));
                }

                rows.push(`(${values.join(',')})`);
            }

            const sql = `INSERT INTO ${table} (${keys.join(',')}) VALUES `;

            db.run(sql + rows.join(','), function (err) {
                if (err) {
                    reject(err.message);
                } else {
                    res = this.lastID;
                }
            });

            db.run('COMMIT', errCb);
            db.run('PRAGMA shrink_memory', errCb);
        });

        db.close(function (err) {
            if (err) {
                reject(err.message);
            } else {
                resolve(res);
            }
        });
    });
}

export function addRowsChunk(table: EDbTables, schema: TSQLiteSchema, items: Array<{[key: string]: any}>): Promise<number> {
    return new Promise(async function (resolve, reject) {
        const length = items.length;
        const chunkSize = 10000;
        // const chunked = items.length > chunkSize;
        let tmpItems = items;
        let start: number;
        let lastId: number;

        if (length > 1000) {
            start = Date.now();
        }

        while (tmpItems.length) {
            const currentItems = tmpItems.slice(0, chunkSize);

            try {
                lastId = await addRows(table, schema, currentItems);

                /*if (chunked) {
                    log('sql addRows', `Chunk step lastID: ${lastId}`);
                }*/
            } catch (e) {
                reject(e);
            }

            tmpItems = tmpItems.slice(chunkSize);
        }

        if (start) {
            log('sql', `Insert ${length} items to "${table}"`, null, start);
        }

        resolve(lastId);
    });
}

export function select<T>(query: string): Promise<T[]> {
    return new Promise(function (resolve, reject) {
        const db = getDb();
        let res: any;

        db.serialize(function () {
            db.all(query, function (err, rows) {
                if (err) {
                    reject(err.message);
                } else {
                    res = rows;
                }
            });

            db.run('PRAGMA shrink_memory');
        });

        db.close(function (err) {
            if (err) {
                reject(err.message);
            } else {
                resolve(res);
            }
        });
    });
}

export function remove(query: string): Promise<number> {
    return new Promise(function (resolve, reject) {
        const db = getDb();
        let res: any;

        db.serialize(function () {
            db.run(query, function (err) {
                if (err) {
                    reject(err.message);
                } else {
                    res = this.changes;
                }
            });

            // Освобождает место на диске, но может вызвать замедление при множественных удалениях
            // Убрать, если будет создавать нагрузку, и перенести в очистку по расписанию, например в cron
            db.run('VACUUM');
        });

        db.close(function (err) {
            if (err) {
                reject(err.message);
            } else {
                resolve(res);
            }
        });
    });
}

export function update(table: EDbTables, schema: TSQLiteSchema, item: {[key: string]: any}): Promise<number> {
    return new Promise(function (resolve, reject) {
        const db = getDb();
        let res: any;

        db.serialize(function () {
            const keys = Object.keys(schema);

            const toSet = [];
            const toSetHash: {[key: string]: any} = {};

            for (const [key, value] of Object.entries(item)) {
                if (!keys.includes(key)) {
                    continue;
                }

                toSet.push(`${key} = $${key}`);
                toSetHash[`$${key}`] = value;
            }

            const query = `UPDATE ${table} SET ${toSet.join(',')} WHERE id = ${item.id}`;

            db.run(query, toSetHash, function (err) {
                if (err) {
                    reject(err.message);
                } else {
                    res = this.changes;
                }
            });
        });

        db.close(function (err) {
            if (err) {
                reject(err.message);
            } else {
                resolve(res);
            }
        });
    });
}

function cover(value: any, type: string) {
    switch (type) {
        case 'TEXT':
            return `'${value}'`;

        default:
            return `${value}`;
    }
}
