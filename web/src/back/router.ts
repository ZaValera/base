import path from 'path';
import express from  'express';
import {IDescription, IItem} from 'shared/consts';


export const router = express.Router();

//--------------- Authorization -----------------------
router.get('/i/want/to/use/this/service', (req, res, next) => {
    res.cookie('auth', 'ICanUseThisService');
    res.redirect('/');
});

router.all('*', (req, res, next) => {
    if (req.cookies['auth'] !== 'ICanUseThisService') {
        res.send('Go away!');
    } else {
        next();
    }
});
//------------------------------------------------------

interface IParams {
    id: number;
}

interface IResBody {
    result: number[];
}

interface IReqBody {
    data: string;
}

interface IReqQuery {
    date: string;
}

interface IError {
    message: string;
    code?: string;
}

router.route('/api/v1/some(/:id)?.json')
    // (req.Params, res.body, req.body, req.query)
    .get<IParams, IResBody|IError, IReqBody, IReqQuery>(async (req, res) => {
        try {
            const {id} = req.params;
            const {data} = req.body;
            const {date} = req.query;

            res.send({result: [1]});
        } catch (e) {
            res.status(400).send({
                message: e,
                code: 'commonError',
            });
        }
    })
    .post<IParams, IResBody|IError, IReqBody, IReqQuery>(async (req, res) => {
        try {
            const {id} = req.params;
            const {data} = req.body;
            const {date} = req.query;

            res.send({result: [1]});
        } catch (e) {
            res.status(400).send({
                message: e,
                code: 'commonError',
            });
        }
    })
    .delete<IParams, IResBody|IError, IReqBody, IReqQuery>(async (req, res) => {
        try {
            const {id} = req.params;
            const {data} = req.body;
            const {date} = req.query;

            res.send({result: [1]});
        } catch (e) {
            res.status(400).send({
                message: e,
                code: 'commonError',
            });
        }
    });

router.route('/api/v1/items.json')
    // (req.Params, res.body, req.body, req.query)
    .get<IParams, IItem[]|IError, IReqBody, IReqQuery>(async (req, res) => {
        try {
            res.send([{
                id: 1,
                date: 1,
                price: 1,
                value: 1,
            }]);
        } catch (e) {
            res.status(400).send({
                message: e,
                code: 'commonError',
            });
        }
    })

router.route('/api/v1/descriptions.json')
    // (req.Params, res.body, req.body, req.query)
    .get<IParams, IDescription[]|IError, IReqBody, IReqQuery>(async (req, res) => {
        try {
            res.send([{
                id: 1,
                description: 'description',
            }]);
        } catch (e) {
            res.status(400).send({
                message: e,
                code: 'commonError',
            });
        }
    })
