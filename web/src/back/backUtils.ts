import fs from 'fs';
import path from 'path';
import {getLogString} from 'shared/utils';
import https from 'https';
import http from 'http';
import os from 'os';

const logFile = fs.createWriteStream(path.resolve(os.tmpdir(), 'base.log'), {flags : 'a+'});

export function log(pref: string, str: string, data?: any, timeStart?: number) {
    const resStr = getLogString(pref, str, timeStart);

    logFile.write(resStr + '\n');
    console.log(resStr);

    if (data) {
        const dataStr = JSON.stringify(data, (key, value) => (value instanceof Map ? [...value] : value));
        logFile.write(dataStr + '\n');
        console.log(dataStr);
    }

    return resStr;
}

export function get(url: string) {
    return new Promise((resolve, reject) => {
        let urlObj;

        try {
            urlObj = new URL(url);
        } catch (e) {
            reject('Invalid URL');
        }

        const client = urlObj.protocol === 'https:' ? https : http;

        client.get(urlObj, (res) => {
            const {statusCode} = res;

            if (statusCode === 302 || statusCode === 301) {
                reject(`Redirect: ${res.headers.location}`);
            } else {
                let data = '';

                res.on('data', (chunk) => {
                    data += chunk;
                });

                res.on('end', () => {
                    resolve(data);
                });
            }
        }).on('error', (err) => {
            reject(err);
        });
    });
}