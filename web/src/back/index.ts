import path from 'path';
import express from 'express';
import cookieParser from 'cookie-parser';
import compression from 'compression';
import {AddressInfo} from 'net';
import {router} from 'back/router';
import {dbInit} from 'back/SQLite';
import {log} from 'back/backUtils';
import {renderIndexPage} from './ssr';


const app = express();

app.use(express.json());
app.use(cookieParser());
app.use(compression());
app.use(router);

app.use('/build', express.static(path.join(__dirname, '../../build/front')));
app.use('/workers', express.static(path.join(__dirname, '../../build/workers')));

const pages = [
    '/',
    '/sub',
];

router.get(pages, renderIndexPage);

log('core', '-----------------------START----------------------------');

const server = app.listen(3000, () => {
    const {port} = server.address() as AddressInfo;

    log('core', `Server listening at ${port} port`);
});

server.timeout = 0;

dbInit(() => {
    // dbInit Callback
});
