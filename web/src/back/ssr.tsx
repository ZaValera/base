import React from 'react';
import path from 'path';
import fs from 'fs';
import {Request, Response} from 'express';
import {renderToString} from 'react-dom/server';
import {StaticRouter} from 'react-router-dom/server';
import {App} from 'front/app';
import {createEmotionCache, theme} from 'shared/mui';
import CssBaseline from '@mui/material/CssBaseline';
import {ThemeProvider} from '@mui/material/styles';
import {CacheProvider} from '@emotion/react';
import createEmotionServer from '@emotion/server/create-instance';

const isDev = process.env.NODE_ENV === 'development';

const cssTemplate = '<!--css-->';
const appTemplate = '<!--app-->';

export function renderIndexPage(req: Request, res: Response) {
    const indexHtml = fs.readFileSync(path.join(__dirname, '../../build/front/index.html'), 'utf8');

    res.status(200);
    res.setHeader('Content-type', 'text/html');

    if (isDev) {
        res.send(indexHtml);
        return;
    }

    const cache = createEmotionCache();
    const {extractCriticalToChunks, constructStyleTagsFromChunks} = createEmotionServer(cache);

    const appHtml = renderToString(
        <StaticRouter location={req.url}>
            <CacheProvider value={cache}>
                <ThemeProvider theme={theme}>
                    <CssBaseline />
                    <App />
                </ThemeProvider>
            </CacheProvider>
        </StaticRouter>
    );

    const emotionChunks = extractCriticalToChunks(appHtml);
    const emotionCss = constructStyleTagsFromChunks(emotionChunks);

    res.send(indexHtml.replace(cssTemplate, emotionCss).replace(appTemplate, appHtml));
}