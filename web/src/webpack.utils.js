const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');


function getCommonLoaders(env) {
    const loaders = [];

    return loaders;
};

function getParams(env) {
    const isDev = process.env.NODE_ENV === 'development';
    const dirname = env && env.dirname || __dirname;
    const storybook = env && env.storybook;

    return {
        isDev,
        dirname,
        storybook,
    };
};

exports.getSettings = function (env, type) {
    const {dirname, isDev} = getParams(env);

    const frontPath = path.resolve(dirname, 'front');
    const backPath = path.resolve(dirname, 'back');
    const sharedPath = path.resolve(dirname, 'shared');
    const workersPath = path.resolve(dirname, 'workers');

    const tsPaths = [frontPath, sharedPath];
    const isBack = type === 'back';
    const isWorkers = type === 'workers';

    if (isBack) {
        tsPaths.push(backPath);
    }

    if (isWorkers) {
        tsPaths.push(workersPath);
    }

    const commonLoaders = getCommonLoaders(env);

    return {
        mode: isDev ? 'development' : 'production',
        devtool: isDev ? 'eval-cheap-module-source-map' : 'source-map',
        cache: true,
        target: type === 'back' ? 'node' : 'web',
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    // exclude: /node_modules/,
                    include: tsPaths,
                    use: [
                        ...commonLoaders,
                        {
                            loader: 'ts-loader',
                            options: {
                                transpileOnly: true,
                                configFile: path.resolve(dirname, `${type}/tsconfig.json`),
                            },
                        },
                    ],
                },
                {
                    test: /\.(png|svg|jpg|gif)$/,
                    include: path.resolve(dirname, 'front/assets/images'),
                    type: 'asset/resource',
                    generator: {
                        filename: 'images/[name]_[hash].[ext]'
                    },
                },
                {
                    test: /\.(eot|ttf|woff)$/,
                    include: path.resolve(dirname, 'front/assets/fonts'),
                    type: 'asset/resource',
                    generator: {
                        filename: 'fonts/[name]_[hash].[ext]'
                    },
                },
                {
                    test: /\.css$/,
                    include: frontPath,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                emit: !isBack,
                            },
                        },
                        ...commonLoaders,
                        'css-loader',
                    ],
                },
                {
                    test: /\.s[ac]ss$/i,
                    include: frontPath,
                    use: [
                        {
                            loader: MiniCssExtractPlugin.loader,
                            options: {
                                emit: !isBack,
                            },
                        },
                        ...commonLoaders,
                        {
                            loader: 'css-loader',
                            options: {
                                sourceMap: isDev,
                                importLoaders: 3,
                                modules: {
                                    localIdentName: isDev ? '[name]--[local]--[hash:base64:6]' : '[hash:base64:6]',
                                    exportLocalsConvention: 'camelCase',
                                },
                            },
                        },
                        {
                            loader: 'resolve-url-loader',
                        },
                        {
                            loader: 'sass-loader',
                            options: {
                                sourceMap: true,
                                implementation: require('sass'),
                                sassOptions: {
                                    includePaths: ['../node_modules'],
                                },
                            },
                        },
                        {
                            loader: 'sass-resources-loader',
                            options: {
                                resources: [
                                    path.resolve(dirname, 'front/styles/mixins.scss'),
                                ],
                            },
                        },
                    ],
                },
            ],
        },
        resolve: {
            extensions: ['.js', '.tsx', '.ts'],
            alias: {
                front: path.resolve(dirname, 'front'),
                back: path.resolve(dirname, 'back'),
                shared: path.resolve(dirname, 'shared'),
                assets: path.resolve(dirname, 'front/assets'), // для import в ts
                './front': path.resolve(dirname, 'front'), // для url() в scss
            },
            symlinks: false,
            cacheWithContext: false,
        },
    };
};

exports.getPlugins = function (env, type) {
    const {dirname} = getParams(env);

    return [
        new MiniCssExtractPlugin({
            filename: 'css/[name].[contenthash].css',
            chunkFilename: 'css/[id].[contenthash].css',
        }),
        new ForkTsCheckerWebpackPlugin({
            typescript: {
                configFile: path.resolve(dirname, `${type}/tsconfig.json`),
            }
        }),
        new FriendlyErrorsWebpackPlugin({
            compilationSuccessInfo: {
                messages: [type.toUpperCase()],
            },
        }),
    ];
};

exports.getParams = getParams;
exports.getCommonLoaders = getCommonLoaders;