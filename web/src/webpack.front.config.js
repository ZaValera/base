const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const {getSettings, getParams, getPlugins} = require('./webpack.utils');


const TYPE = 'front';

module.exports = env => {
    const {
        dirname,
        storybook,
    } = getParams(env);

    const plugins = getPlugins(env, TYPE);

    if (!storybook) {
        plugins.push(new HtmlWebpackPlugin({
            inject: true,
            filename: 'index.html',
            minify: {
                collapseWhitespace: false,
                removeComments: false,
            },
            favicon: path.resolve(dirname, './front/assets/images/base.png'),
            template: path.resolve(dirname, './front/index.html'),
            title: 'Base',
        }));
    }

    return {
        ...getSettings(env, TYPE),
        plugins,
        entry: './src/front/index.tsx',
        output: {
            filename: '[id].[contenthash].js',
            chunkFilename: '[id].[contenthash].js',
            path: path.resolve(dirname, '../build/front'),
            publicPath: '/build/',
        },
        optimization: {
            splitChunks: {
                chunks: 'all',
            },
        },
    };
};
